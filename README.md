**coding style**

http://httpd.apache.org/dev/styleguide.html

    $ indent -i4 -npsl -di0 -br -nce -d0 -cli0 -npcs -nfc1 -nut
    $ cppcheck --enable=all

**usage**

	$ make
	$ for i in $(seq 0 10); do ./mq_send; done
	$ ./mq_recv

**change msg_max**

	$ echo [num] > /proc/sys/fs/mqueue/msg_max
