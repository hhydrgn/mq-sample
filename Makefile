targets = mq_send mq_recv

all: $(targets)

%: %.c
	gcc -g -lrt -o $@ $<

clean:
	rm -f $(targets)
