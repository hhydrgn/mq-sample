#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <mqueue.h>

int main()
{
    mqd_t mqdes;
    char message[256] = { 0 };
    struct mq_attr attr = {.mq_maxmsg = 10,.mq_msgsize = sizeof(message) };

    if ((mqdes = mq_open("/mq_test", O_WRONLY | O_CREAT, 0644, &attr)) >= 0) {
        struct timespec timeout = {.tv_sec = 1,.tv_nsec = 0 };

        if (mq_getattr(mqdes, &attr)) {
            perror("mq_getattr()");
            goto err;
        }
        sprintf(message, "Hello MQ [%d]", attr.mq_curmsgs);
        if (mq_timedsend(mqdes, message, strlen(message), 1, &timeout)) {
            perror("mq_timedsend()");
            goto err;
        }
    }
    else {
        perror("mq_open()");
        goto err;
    }

    return 0;
  err:
    return 1;
}
