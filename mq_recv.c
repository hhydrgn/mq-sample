#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <mqueue.h>

int main()
{
    mqd_t mqdes;

    if ((mqdes = mq_open("/mq_test", O_RDONLY)) >= 0) {
        int i = 0, size, prio;
        char message[256] = { 0 };
        struct mq_attr attr;
        struct timespec timeout = {.tv_sec = 1,.tv_nsec = 0 };

        if (mq_getattr(mqdes, &attr)) {
            perror("mq_getattr()");
            goto err;
        }
        while ((size =
                mq_timedreceive(mqdes, message, attr.mq_msgsize, &prio,
                                &timeout)) > 0) {
            printf("%d %d %s\n", ++i, size, message);
            memset(message, 0, sizeof(message));
        }
        if (mq_close(mqdes)) {
            perror("mq_close()");
            goto err;
        }
        if (mq_unlink("/mq_test")) {
            perror("mq_unlink()");
            goto err;
        }
    }
    else {
        perror("mq_open()");
        goto err;
    }

    return 0;
  err:
    return 1;
}
